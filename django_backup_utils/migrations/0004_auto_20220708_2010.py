# Generated by Django 3.2.3 on 2022-07-08 18:10

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('django_backup_utils', '0003_auto_20220707_2028'),
    ]

    operations = [
        migrations.RenameField(
            model_name='backup',
            old_name='system_migration_files',
            new_name='dump_migration_files',
        ),
        migrations.RemoveField(
            model_name='backup',
            name='system_version',
        ),
        migrations.AddField(
            model_name='backup',
            name='consistent_migrations',
            field=models.BooleanField(default=False, verbose_name='Consistent Migrations'),
        ),
        migrations.AlterField(
            model_name='backup',
            name='system_migrations_migrated',
            field=models.IntegerField(blank=True, null=True, verbose_name='System Migrations (at dump time)'),
        ),
    ]
